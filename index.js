const express = require('express'),
  mysql = require('mysql'),
  app = express(),
  cors = require('cors'),
  bodyParser = require('body-parser'),
  env = require('dotenv').config();
  
// setup database
var environment;
switch(process.env.APP_ENV){
  case 'local':
  case 'development':
    environment = {
      app: process.env.APP_PORT_DEVELOPMENT,
      host: process.env.DB_HOST_DEVELOPMENT,
      user: process.env.DB_USER_DEVELOPMENT,
      password: process.env.DB_PASSWORD_DEVELOPMENT,
      database: process.env.DB_NAME_DEVELOPMENT
    }
    break;

  case 'staging':
    environment = {
      app: process.env.APP_PORT_STAGING,
      host: process.env.DB_HOST_STAGING,
      user: process.env.DB_USER_STAGING,
      password: process.env.DB_PASSWORD_STAGING,
      database: process.env.DB_NAME_STAGING
    }
    break;

  case 'production':
    environment = {
      app: process.env.APP_PORT_PRODUCTION,
      host: process.env.DB_HOST_PRODUCTION,
      user: process.env.DB_USER_PRODUCTION,
      password: process.env.DB_PASSWORD_PRODUCTION,
      database: process.env.DB_NAME_PRODUCTION
    }
    break;
  
  default:
    environment = {
      app: process.env.APP_PORT_DEVELOPMENT,
      host: process.env.DB_HOST_DEVELOPMENT,
      user: process.env.DB_USER_DEVELOPMENT,
      password: process.env.DB_PASSWORD_DEVELOPMENT,
      database: process.env.DB_NAME_DEVELOPMENT
    }

}
db = mysql.createConnection(environment)  

// make server object that contain port property and the value for our server.
// var server = {
//   port: 8000
// };

const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');

// use the modules
app.use(cors())
app.use(bodyParser.json());


// use router
app.use(express.static('public'))
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

// starting the server
app.listen( environment.app , () => console.log(`Server running, at connection ${environment.host} listening port: ${environment.app}`));

// to run apps
// nodemon ./server.js localhost 8080