// belum di pake

require('dotenv').config();
const env = process.env.APP_ENV; // 'dev' or 'test'

const dev = {
    app: {
        server: env,
        port: parseInt(process.env.APP_PORT_DEVELOPMENT) || 3000
    },
    db: {
        host: process.env.DB_HOST_DEVELOPMENT || 'localhost',
    //   port: parseInt(process.env.DEV_DB_PORT) || 27017,
        user: process.env.DB_USER_DEVELOPMENT,
        name: process.env.DB_NAME_DEVELOPMENT || 'db',
        password: process.env.DB_PASSWORD_DEVELOPMENT
    }
   };

   const prod = {
    app: {
        server: env,
        port: parseInt(process.env.APP_PORT_PRODUCTION) || 3000
    },
    db: {
        host: process.env.DB_HOST_PRODUCTION || 'localhost',
    //   port: parseInt(process.env.TEST_DB_PORT) || 27017,
        user: process.env.DB_USER_PRODUCTION,
        name: process.env.DB_NAME_PRODUCTION || 'test',
        password: process.env.DB_PASSWORD_PRODUCTION
    }
   };
   
   const config = {
    dev,
    prod
   };

module.exports = config[env];