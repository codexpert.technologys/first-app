const express = require('express'),
  mysql = require('mysql'),
  app = express(),
  cors = require('cors'),
  bodyParser = require('body-parser'),
  // env = require('dotenv').config();
  config = require('./config');
  
// setup database
// var environment;
// switch(process.env.APP_ENV){
//   case 'local':
//   case 'development':
//     environment = {
//       host: process.env.DB_HOST_DEVELOPMENT,
//       user: process.env.DB_USER_DEVELOPMENT,
//       password: process.env.DB_PASSWORD_DEVELOPMENT,
//       database: process.env.DB_NAME_DEVELOPMENT
//     }
//     break;

//   case 'staging':
//     environment = {
//       host: process.env.DB_HOST_STAGING,
//       user: process.env.DB_USER_STAGING,
//       password: process.env.DB_PASSWORD_STAGING,
//       database: process.env.DB_NAME_STAGING
//     }
//     break;

//   case 'production':
//     environment = {
//       host: process.env.DB_HOST_PRODUCTION,
//       user: process.env.DB_USER_PRODUCTION,
//       password: process.env.DB_PASSWORD_PRODUCTION,
//       database: process.env.DB_NAME_PRODUCTION
//     }
//     break;
  
//   default:
//     environment = {
//       host: process.env.DB_HOST_DEVELOPMENT,
//       user: process.env.DB_USER_DEVELOPMENT,
//       password: process.env.DB_PASSWORD_DEVELOPMENT,
//       database: process.env.DB_NAME_DEVELOPMENT
//     }

// }
db = mysql.createConnection(config.db)  

// make server object that contain port property and the value for our server.
// var server = {
//   port: 8000
// };

const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');

// use the modules
app.use(cors())
app.use(bodyParser.json());


// use router
app.use(express.static('public'))
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

// starting the server
app.listen( config.app.server , () => console.log(`Server running, at connection ${config.app.server} listening port: ${config.app.server}`));

// to run apps
// nodemon ./server.js localhost 8080